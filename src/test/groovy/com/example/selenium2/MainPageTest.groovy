package com.example.selenium2

import org.junit.jupiter.api.BeforeAll
import org.openqa.selenium.Keys
import org.openqa.selenium.firefox.FirefoxDriver
import org.testng.annotations.*
import static org.testng.Assert.*
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.interactions.Actions

import java.util.concurrent.TimeUnit

class MainPageTest {
    private WebDriver driver
    private MainPage mainPage


    @BeforeMethod
    void setUp() {
        WebDriverManager.chromedriver().setup()
        driver = new ChromeDriver()
        driver.manage().window().maximize()
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
        driver.get("https://dtf.ru/")
        mainPage = new MainPage(driver)
    }

    @AfterMethod
    void tearDown() {
        driver.quit()
    }
    @Test
    void searchValidValue() {

        mainPage.sendValueSearchField(ConfProperties.getProperty("search_word1") + String.valueOf(Keys.ENTER))
        assertEquals(mainPage.searchResultText(), ConfProperties.getProperty("search_word1"))
    }
    @Test
    void searchNotValidValue() {

        mainPage.sendValueSearchField(ConfProperties.getProperty("search_word2")+ String.valueOf(Keys.ENTER))
        assertEquals(mainPage.searchResultText(), ConfProperties.getProperty("search_word2"))
        WebElement emptySearchResult = driver.findElement(By.xpath("//*[@class = 'search_results__dummy__message l-fs-14 l-lh-20 l-mh-auto l-ta-center']"))
        assertEquals(emptySearchResult.getText(), "�� ��� ����������� ����������, �� ������ �� ����� :(\n" +
                "����� ����������� �������� ��������� ������ ��� �������� ���-�� �� �������.")
    }
    @Test
    void mainPageUrl() {

        assertEquals(driver.getCurrentUrl(), "https://dtf.ru/" )

    }
    @Test
    void PlusPageUrl() {

        mainPage.sponsorsButtonClick()
        assertEquals(driver.getCurrentUrl(), "https://dtf.ru/plus" )

    }

   /* @Test
    void search() {
        mainPage.searchButton.click()

        WebElement searchField = driver.findElement(By.cssSelector("[data-test='search-input']"))
        searchField.sendKeys("Selenium")

        WebElement submitButton = driver.findElement(By.cssSelector("button[data-test='full-search-button']"))
        submitButton.click()

        WebElement searchPageField = driver.findElement(By.cssSelector("input[data-test='search-input']"))
        assertEquals(searchPageField.getAttribute("value"), "Selenium")
    }*/

  /*  @Test
    void toolsMenu() {
        new Actions(driver)
                .moveToElement(mainPage.toolsMenu)
                .perform()

        WebElement menuPopup = driver.findElement(By.cssSelector("div[data-test='menu-main-popup-content']"))
        assertTrue(menuPopup.isDisplayed())
    }*/


}
