package com.example.selenium2

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

// page_url = https://www.jetbrains.com/
class MainPage {


    @FindBy(xpath = "//label[contains(@class,'v-text-input v-text-input--default')]")
    private WebElement searchField

    @FindBy(xpath = "//div[@data-test='main-menu-item' and @data-test-marker = 'Developer Tools']")
    private WebElement searchButton

    @FindBy(xpath = "//div[@class = 'v-header-title__main']")
    private WebElement searchResult

    @FindBy(xpath = "//*[@class = 'sidebar__footer__item sidebar__footer__item--sponsor']//child::span")
    private WebElement sponsorsButton

    MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this)
    }

    void sendValueSearchField(String search_word)
    {
        searchField.sendKeys(search_word)
    }
    void sponsorsButtonClick() {
        sponsorsButton.click()
    }
    String searchResultText() {
        return searchResult.getText()
    }

}
